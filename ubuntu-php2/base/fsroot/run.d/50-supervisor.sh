#!/bin/bash
##
# Supervisor support using environment variables
#
# Usage:
#   `<user> <numprocs> <command>``
#
# Parameters:
#   user: The uid/username to execute the command
#   numprocs: The number of processes to invoke (useful when running
#             workers that need multiple instances running concurrently)
#   command: The command to run
#
# Example:
# - SUPERVISOR_TAIL=www-data 1 tail -f /dev/null
##

set -e

SUPERVISORS=$(env | grep SUPERVISOR_ | sed -r 's/^SUPERVISOR_([A-Z0-9_]+)=(.*)$/\1 \2/')

if [ -z "$SUPERVISORS" ]; then
    exit
fi

IFS=$'\n'
for SUPERVISOR in $SUPERVISORS; do
    set -f
    IFS=' '
    SUPERVISOR=($SUPERVISOR)
    NAME="${SUPERVISOR[0]}"
    USER="${SUPERVISOR[1]}"
    NUMPROCS="${SUPERVISOR[2]}"
    COMMAND="${SUPERVISOR[@]:3}"
    CONFIG="/etc/supervisord.d/${NAME}.ini"

    touch $CONFIG
    truncate -s0 $CONFIG

    echo "[program:${NAME}]" >> $CONFIG
    echo "user=${USER}" >> $CONFIG
    echo "numprocs=${NUMPROCS}" >> $CONFIG
    echo "process_name=%(program_name)s_%(process_num)02d" >> $CONFIG
    echo "command=${COMMAND}" >> $CONFIG
    echo "stdout_logfile=/var/log/supervisor/%(program_name)s_%(process_num)02d.log" >> $CONFIG
    echo "redirect_stderr=true" >> $CONFIG
    echo "autorestart=true" >> $CONFIG
    echo "autostart=true" >> $CONFIG
    echo "startretries=99999" >> $CONFIG
    set +f
done

supervisord -c /etc/supervisord.conf
