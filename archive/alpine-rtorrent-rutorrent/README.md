Docker Image for (r|ru)torrent
===

This currently a bare-bones image containing rtorrent and rutorrent.

Downloads, rtorrent cache, and rutorrent cache are placed into /downloads.

Source is available at https://gitlab.com/adduc-projects/docker-images/alpine-rtorrent-rutorrent

### Volumes

* /data/rtorrent
* /data/rutorrent


### Comparison of (r|ru)torrent containers

Image                        | Size                  | Basic Auth | Custom Memory Limit
--- | --- | --- | ---
[adduc/alpine-rtorrent-rutorrent] | [![][img-adduc]][badge-adduc] | No | No
[linuxserver/rutorrent] | [![][img-linuxserver]][badge-linuxserver] | No | No
[diameter/rtorrent-rutorrent] | [![][img-diameter]][badge-diameter] | Yes | Yes


[adduc/alpine-rtorrent-rutorrent]: https://hub.docker.com/r/adduc/alpine-rtorrent-rutorrent/
[linuxserver/rutorrent]: https://hub.docker.com/u/linuxserver/rutorrent/
[diameter/rtorrent-rutorrent]: https://hub.docker.com/u/diameter/rtorrent-rutorrent

[img-adduc]: https://images.microbadger.com/badges/image/adduc/alpine-rtorrent-rutorrent.svg
[badge-adduc]: https://microbadger.com/images/adduc/alpine-rtorrent-rutorrent

[img-linuxserver]: https://images.microbadger.com/badges/image/linuxserver/rutorrent.svg
[badge-linuxserver]: https://microbadger.com/images/linuxserver/rutorrent

[img-diameter]: https://images.microbadger.com/badges/image/diameter/rtorrent-rutorrent.svg
[badge-diameter]: https://microbadger.com/images/diameter/rtorrent-rutorrent
