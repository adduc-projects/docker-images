#!/bin/bash

running_in_docker() {
  (awk -F/ '$2 == "docker"' /proc/self/cgroup | read non_empty_input)
}

if ! running_in_docker; then
    echo "This does not appear to be a docker container; refusing to run."
    exit 1
fi

mkdir -p /var/log/php /var/run/php
chown 1000 /var/log/php -R

for FILE in /run.d/*.sh; do
  chmod +x $FILE
  $FILE
done

while true; do
  sleep 10 &
  wait $!
done
