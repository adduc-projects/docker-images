#!/bin/bash

IFS=$'\n'
POOLS=$(env | grep POOL_ | sed -r 's/^POOL_([A-Z0-9_]+)=(.*)$/\1 \2/')
for POOL in $POOLS; do
    IFS=' '
    POOL=($POOL)

    INCLUDE_PATH_VAR="${POOL[0]}_INCLUDE_PATH"
    INCLUDE_PATH="${!INCLUDE_PATH_VAR:-.}"

    SRC=/opt/php-fpm.pool.conf
    DEST=/etc/php/${PHP_VERSION}/fpm/pool.d/${POOL[0]}.conf

    cp "$SRC" "$DEST"

    sed -i "s#{{HOST}}#${POOL[0]}#g" $DEST
    sed -i "s#{{PORT}}#${POOL[1]}#g" $DEST
    sed -i "s#{{MAX_CHILDREN}}#${POOL[2]:-20}#g" $DEST
    sed -i "s#{{INCLUDE_PATH}}#${INCLUDE_PATH}#g" $DEST

    touch "/var/log/php/${POOL[0]}.access.log"
    touch "/var/log/php/${POOL[0]}.error.log"
    touch "/var/log/php/${POOL[0]}.slow.log"
done

chown 1000:1000 /var/log/php -R

php-fpm${PHP_VERSION}
