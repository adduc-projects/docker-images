#!/bin/bash

if [ -f "requirements.yml" ]; then
    ansible-galaxy install -r requirements.yml
fi

ansible-playbook playbook.yml
