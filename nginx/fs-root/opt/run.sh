#!/bin/bash

mkdir -p /etc/nginx/templates

HOSTS=$(env | grep HOST_ | sed -r 's/^HOST_([A-Z0-9_]+)=(.*)$/\1 \2/')

# shellcheck source=fs-root/opt/functions.sh
source /opt/functions.sh

generate_resolvers

IFS=$'\n'
for HOST in $HOSTS; do
  IFS=" " read -r -a HOST <<< "$HOST"

  SRC="/opt/templates/${HOST[2]}.conf"
  DEST="/etc/nginx/templates/${HOST[1]}.conf"

  cp "$SRC" "$DEST"

  sed -i "s#{{VHOST}}#${HOST[1]}#" "$DEST"

  check_crawlable
  check_cacheable
  check_maintenance

  "host_${HOST[2]}"

  https
done

cp /opt/nginx.conf /etc/nginx/nginx.conf

sed -i "s#{{CPU_COUNT}}#$(grep -c processor /proc/cpuinfo)#" /etc/nginx/nginx.conf
sed -i "s#{{MAX_BODY_SIZE}}#${MAX_BODY_SIZE:-100m}#" /etc/nginx/nginx.conf
nginx
